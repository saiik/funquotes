<?php
/**
 * FUNQuotes Template
 *
 * @category  FUNQuotes/Helper/BBCode
 * @package   FUNQuotes/Class/BBCode
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Class_BBCode {

            var $parser;

            /**
             * function BBParser
             * 
             * @param type $mes
             * @return type
             */
            function BBParser($mes) {
                $new = $this->BBCode($mes);
                $new = $this->flag($new);
                $new = $this->Quote($new);
                $new = $this->listul($new);
                $new = $this->spoiler($new);
                $new = $this->align($new);
                $new = $this->youtube($new);
                $new = $this->center($new);
                $new = $this->info($new);
                $new = $this->smil($new);
                return $new;
            }

            /**
             * function BBCode
             * 
             * @param type $mes
             * @return type
             */
            function BBCode($mes) {
                    $mes = preg_replace('/\[b\](.*?)\[\/b\]/', '<b>$1</b>', $mes);
                    $mes = preg_replace('/\[i\](.*?)\[\/i\]/', '<i>$1</i>', $mes);
                    $mes = preg_replace('/\[B\](.*?)\[\/B\]/', '<b>$1</b>', $mes);
                    $mes = preg_replace('/\[I\](.*?)\[\/I\]/', '<i>$1</i>', $mes);
                    $mes = preg_replace('/\[u\](.*?)\[\/u\]/', '<u>$1</u>', $mes);
                    $mes = preg_replace('/\[U\](.*?)\[\/U\]/', '<u>$1</u>', $mes);
                    $mes = preg_replace('/\[left\](.*?)\[\/left\]/', '<div style="text-align: left;">$1</div>', $mes);
                    $mes = preg_replace('/\[right\](.*?)\[\/right\]/', '<div style="text-align: right;">$1</div>', $mes);
                    $mes = preg_replace('/\[center\](.*?)\[\/center\]/', '<div style="text-align: center;">$1</div>', $mes);
                    $mes = preg_replace('/\[color=(.*?)](.*)\[\/color\]/', '<font style="color: $1">$2</font>', $mes);
                    $mes = preg_replace('/\[size=(.*?)](.*)\[\/size\]/', '<font style="font-size: $1px">$2</font>', $mes);
                    $mes = preg_replace('/\[font=(.*?)](.*)\[\/font\]/', '<font style="font-family: $1">$2</font>', $mes);
                    $mes = preg_replace('/\[img\](.*)\[\/img\]/', '<img src="$1" alt="" />', $mes);
                    $mes = preg_replace('/\[url=([^ ]+).*\](.*)\[\/url\]/', '<a href="$1">$2</a>', $mes);
                    $mes = preg_replace('/\n/', "<br />\n", $mes);

                    return $mes;
            }

            /**
             * function Quote
             * 
             * @param type $mes
             * @param type $in
             * @return type
             */
            function Quote($mes, $in="") {
                    if(empty($in)) {
                            $mes = str_replace('[quote]', '<div class="zitat">Zitat:<hr />', $mes);
                            $mes = str_replace('[/quote]', '</div>', $mes);
                            return $mes;
                    } else {
                            $mes = str_replace('[quote]', '<div class="zitat">Zitat von <i>'.$in.'</i>:<hr />', $mes);
                            $mes = str_replace('[/quote]', '</div>', $mes);
                            return $mes;
                    }
            }

            /**
             * function listul
             * 
             * @param type $mes
             * @return type
             */
            function listul($mes) {
                    $mess = str_replace('[list]', '<ul>', $mes);
                    $mess = str_replace('[*]', '<li>', $mess);
                    $mess = str_replace('[/*]', '</li>', $mess);
                    $mess = str_replace('[/list]', '</ul>', $mess);

                    return $mess;
            }


            /**
             * function spoiler
             * 
             * @param type $link
             * @return type
             */
            function spoiler($link) {
                    $mes = preg_replace('/\[spoiler=([^ ]+).*\](.*)\[\/spoiler\]/', '<a href="#" onclick="spoiler();">$1</a><br /><div id="spoiler">$2</div>', $link);
                    return $mes;
            }

            /**
             * function algn
             * 
             * @param type $link
             * @return type
             */
            function align($link) {
                    $mes = preg_replace('/\[align=([^ ]+).*\](.*)\[\/align\]/', '<div style="text-align: $1;" >$2</div>', $link);
                    return $mes;
            }

            /**
             * function youtube
             * 
             * @param type $link
             * @return type
             */
            function youtube($link) {
                    $mes = preg_replace('/\[youtube\](.*?)\[\/youtube\]/', '<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/$1&hl=de&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/$1&hl=de&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>', $link);
                    return $mes;
            }

            /**
             * function center
             * 
             * @param type $link
             * @return type
             */
            function center($link) {
                    $mes = preg_replace('/\[center\](.*?)\[\/center\]/', '<center>$1</center>', $link);
                    return $mes;
            }

            /**
             * function smil
             * 
             * @param type $mes
             * @return type
             */
            function smil($mes) {
                    $mes = str_replace(':)', '<img src="template/default/resources/images/sm/1.png" alt="" title=":)" />', $mes);
                    $mes = str_replace(':D', '<img src="template/default/resources/images/sm/4.png" alt="" title=":D" />', $mes);
                    $mes = str_replace(':(', '<img src="template/default/resources/images/sm/3.png" alt="" title=":(" />', $mes);
                    $mes = str_replace(':P', '<img src="template/default/resources/images/sm/5.png" alt="" title=":P" />', $mes);
                    $mes = str_replace('<3', '<img src="template/default/resources/images/sm/6.png" alt="" title="<3" />', $mes);
                    $mes = str_replace('^^', '<img src="template/default/resources/images/sm/7.png" alt="" title="^^" />', $mes);
                    $mes = str_replace(';)', '<img src="template/default/resources/images/sm/2.png" alt="" title=";)" />', $mes);

                    return $mes;
            }

            /**
             * function info
             * 
             * @param type $mes
             * @return type
             */
            function info($mes) {
                    $mes = preg_replace('/\[info\](.*?)\[\/info\]/', '<div class="info">$1</div>', $mes);
                    return $mes;
            }

            /**
             * function flag
             * 
             * @param type $flagge
             * @return type
             */
            function flag($flagge) {
                    $flagge = preg_replace('/\[flag=(.*?)]/', '<img src="templates/default/images/flags/$1.png" alt="" />', $flagge);	
                    return $flagge;
            }

    }