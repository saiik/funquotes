<?php
/**
 * FUNQuotes Template
 *
 * @category  FUNQuotes/Helper/Page
 * @package   FUNQuotes/Class/Page
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Class_Page {
        public function __construct() {
            return;
        }
        
        public function getActive($site) {
            $action = $_GET["site"];

                switch($action) {
                    case "quotes":
                        if($site == $action) return "active";
                    break;
                    case "pictures":
                        if($site == $action) return "active";
                    break;
                    case "forum":
                        if($site == $action) return "active";
                    break;
                    case "chat":
                        if($site == $action) return "active";
                    break;
                    case "groups":
                        if($site == $action) return "active";
                    break;
                    case "user":
                        if($site == $action) return "active";
                    break;
                    case "forum":
                        if($site == $action) return "active";
                    break;
                    default:
                        if($site == $action) return "active";
                    break;
                }
        }
        
        public function getPageTitle() {
            $task = $_GET["site"];
                switch($task) {
                    case "quotes":
                        return "Quotes";
                    break;
                    case "pictures":
                        return "Pictures";
                    break;
                    case "forum":
                        return "Forum";
                    break;
                    case "chat":
                        return "Chat";
                    break;
                    case "groups":
                        return "Groups";
                    break;
                    case "user":
                        return "User";
                    break;
                    case "forum":
                        return "Forum";
                    break;
                    default:
                        return "Home";
                    break;
                }
        }
    }