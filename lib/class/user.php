<?php
/**
 * FUNQuotes Template
 *
 * @category  FUNQuotes/Helper/User
 * @package   FUNQuotes/Class/User
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Class_User extends FUNQuotes {
        var $db;
        
        public function __construct(FUNQuotes_Engine_Database_MySQL $db) {
            $this->db = $db;
            return;
        }
        /**
         * @public getUserName
         * 
         * @param type $userID
         * @return type
         */
        public function getUserName($userID) {
            if(empty($userID)) return die("empty userid");
            
            $user = $this->db->fetchRow("SELECT username FROM fun_users WHERE id = '?'", array($userID));
            
            return $user["username"];
        }
    }
