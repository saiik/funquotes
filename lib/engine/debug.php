<?php
/**
 * FUNQuotes Events
 *
 * @category  FUNQuotes/Debug
 * @package   FUNQuotes/Engine/Debug
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */
    require_once 'lib/smarty/Smarty.php';

    class FUNQuotes_Engine_Debug extends FUNQuotes {
        
        /**
         * @var type events
         */
        var $events;
        
        /**
         * @var type db
         */
        var $db;
        
        /**
         * @var type session
         */
        var $session;
        
        /**
         * @var type template
         */
        var $template;
        
        /**
         * @public __construct
         * 
         * @param FUNQuotes_Engine_Events $events
         * @param FUNQuotes_Engine_Database_MySQL $db
         * @param FUNQuotes_Engine_Session $session
         * @return type
         * @throws Exception
         */
        public function __construct(FUNQuotes_Engine_Template_Engine $tpl, FUNQuotes_Engine_Events $events, FUNQuotes_Engine_Database_MySQL $db, FUNQuotes_Engine_Session $session) {
            if(is_null($events)) {
                throw new Exception("event handler not found");
            }
            $this->events = $events;
            
            if(is_null($db)) {
                throw new Exception("database engine not found");
            }
            $this->db = $db;
            
            if(is_null($session)) {
                throw new Exception("session handler not found");
            }
            $this->session = $session;

            if(is_null($tpl)) {
                throw new Exception("template engine not found");
            }
            $this->template = $tpl;
            
            $this->template->template->assign("sql_fetch_stack", $this->db->sqlFetchStack);
            $this->template->template->assign("sql_data_stack", $this->db->sqlArrayStack);
            $this->template->template->assign("session_stack", $_SESSION);
            $this->template->template->assign("event_lists", $this->events->eventList);
            $this->template->template->assign("event_events", $this->events->_callbacks);
            
            $request["GET"][]       = $_GET;
            $request["POST"][]      = $_POST;
            $request["REQUEST"][]   = $_REQUEST;
            
            $this->template->template->assign("request_stack", $request);
            $this->template->template->assign("server_stack", $_SERVER);
            
            $this->template->template->setTemplateDir('template/default/');
            $this->template->template->display("debug.tpl");
            
            return;
        }
    }