<?php
/**
 * FUNQuotes Database
 *
 * @category  FUNQuotes/Database
 * @package   FUNQuotes/Engine/Database/MySQL
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

	class FUNQuotes_Engine_Database_MySQL {
                /**
                 * $host
                 * @var type 
                 */
                private $host;
                
                /**
                 * $user
                 * @var type 
                 */
                private $user;
                
                /**
                 * $pass
                 * @var type 
                 */
                private $pass;
                
                /**
                 * $base
                 * @var type 
                 */
                private $base;
                
                /**
                 * $sqlStatement
                 * @var type 
                 */
                private $sqlStatement;
                
                /**
                 * $sqlData
                 * @var type 
                 */
                private $sqlData;
                
                /**
                 * $sqlMagic
                 * @var type 
                 */
                private $sqlMagic;
                
                /**
                 * $sqlArray
                 * @var type 
                 */
                private $sqlArray;
                
                /**
                 * $sqlQuery
                 * @var type 
                 */
                private $sqlQuery;
                
                /**
                 * $sqlResult
                 * @var type 
                 */
                private $sqlResult;
                
                /**
                 * $sqlDs
                 * @var type 
                 */
                private $sqlDs;
                
                /**
                 * $sqlDataStack
                 * @var type 
                 */
                private $sqlDataStack;
                
                /**
                 * @var type sqlArrayStack
                 */
                public $sqlArrayStack;
                
                /**
                 * @var type sqlFetchStack
                 */
                public $sqlFetchStack;
                
                /**
                 * @var type sqlCount
                 */
                public $sqlCount = 1;
                
                /**
                 * $sec
                 * @var type 
                 */
		public static $sec = array("1=1", "root", "mysql", "truncate", "drop database", "TRUNCATE", "DROP DATABASE");
		
                /**
                 * function __construct
                 * 
                 * @param type $host
                 * @param type $user
                 * @param type $pass
                 * @param type $base
                 */
		public function __construct($host="",$user="",$pass="",$base="") {
			$this->host = $host;
			$this->user = $user;
			$this->pass = $pass;
			$this->base = $base;
			
			if(@mysql_connect($this->host, $this->user, $this->pass)) {
				if(!@mysql_select_db($this->base)) {
					die("Can't select database");
				}
			} else {
				die("Can't connect to database");
			}
		}
		
                /**
                 * function close
                 * 
                 */
		public static function close() {
			@mysql_close();
		}
		
                /**
                 * function query
                 * 
                 * @param type $sql
                 * @param boolean $safe
                 * @return type
                 */
		public function query($sql,$safe=true) {
			if($safe === true) {
				return self::safeQuery($sql);
			} else {
				$return = mysql_query($sql) or die(mysql_error()." - QUERY ABFRAGE");
				return $return;
			}
		}
		
                /**
                 * function fetchAll
                 * 
                 * @param type $fetch
                 * @param boolean $safe
                 * @return type
                 * 
                 * @final
                 */
		public function fetchAll($fetch, $data=array(), $safe=true) {
                    if(!empty($data)) {
                        $this->sqlFetchStack["sql_fetch_".$this->sqlCount] = $fetch;
                        
                        $this->sqlStatement = $fetch;
                        $this->sqlData      = $data;

                        $this->sqlMagic     = substr_count($this->sqlStatement, "?");
                        $this->sqlMagic     = $this->sqlMagic - 1;

                        $this->sqlArray[]   = explode("?", $this->sqlStatement);
                        $this->sqlQuery     = '';
                        $i                  = 0;
                        $sql_count          = count($this->sqlArray);

                        foreach($this->sqlArray as $val) {
                            foreach($val as $key) {
                                $this->sqlQuery .= $val[$i].$this->sqlData[$i];
                                $i++;
                            }
                        }

                        if($safe === true) {
                            $this->sqlResult = self::safeQuery($this->sqlQuery);
                            unset($this->sqlDataStack);
                            while($this->sqlDs = mysql_fetch_assoc($this->sqlResult)) {
                                $this->sqlDataStack[] = $this->sqlDs;
                            }
                            
                            $this->sqlArrayStack["sql_data_".$this->sqlCount] = $this->sqlDataStack;
                            $this->sqlCount = $this->sqlCount+1;
                            
                            return $this->sqlDataStack;
                        } else {
                            $this->sqlResult = mysql_query($this->sqlQuery);
                            unset($this->sqlDataStack);
                            while($this->sqlDs = mysql_fetch_assoc($this->sqlResult)) {
                                $this->sqlDataStack[] = $this->sqlDs;
                            }
                            
                            $this->sqlArrayStack["sql_data_".$this->sqlCount] = $this->sqlDataStack;
                            $this->sqlCount = $this->sqlCount+1;
                            
                            return $this->sqlDataStack;
                        }
                    } elseif(empty($data)) {
                        if(empty($fetch)) die("invalid sql statement");
                        $this->sqlFetchStack["sql_fetch_".$this->sqlCount] = $fetch;
                        
                        $this->sqlQuery = $fetch;
                        unset($this->sqlDataStack);
                        if($safe === true) {
                            $this->sqlResult = self::safeQuery($this->sqlQuery);
                            while($this->sqlDs = mysql_fetch_assoc($this->sqlResult)) {
                                $this->sqlDataStack[] = $this->sqlDs;
                            }

                            $this->sqlArrayStack["sql_data_".$this->sqlCount] = $this->sqlDataStack;
                            $this->sqlCount = $this->sqlCount+1;
                            
                            return $this->sqlDataStack;
                        } else {
                            $this->sqlResult = mysql_query($this->sqlQuery);
                            while($this->sqlDs = mysql_fetch_assoc($this->sqlResult)) {
                                $this->sqlDataStack[] = $this->sqlDs;
                            }

                            $this->sqlArrayStack["sql_data_".$this->sqlCount] = $this->sqlDataStack;
                            $this->sqlCount = $this->sqlCount+1;
                            
                            return $this->sqlDataStack;
                        }
                    }
		}
                
                /**
                 * function fetchRow()
                 * 
                 * @param type $fetch
                 * @param type $data
                 * @param boolean $safe
                 * @return type
                 * 
                 * @final
                 */
                public function fetchRow($fetch, $data=array(), $safe=true) {
                    if(!empty($data)) {
                        unset($this->sqlDataStack);
                        unset($this->sqlQuery);
                        
                        $this->sqlFetchStack[] = $fetch;
                        
                        $this->sqlStatement = $fetch;
                        $this->sqlData      = $data;

                        $this->sqlMagic     = substr_count($this->sqlStatement, "?");
                        $this->sqlMagic	    = $this->sqlMagic - 1;

                        $this->sqlArray[]   = explode("?", $this->sqlStatement);
                        $this->sqlQuery	    = '';
                        $i		    = 0;
                        $sql_count          = count($this->sqlArray);

                        foreach($this->sqlArray as $val) {
                            foreach($val as $key) {
                                $this->sqlQuery .= $val[$i].$this->sqlData[$i];
                                $i++;
                            }
                        }
                        if($safe === true) {
                            $this->sqlResult = self::safeQuery($this->sqlQuery);
                            
                            $this->sqlArrayStack[] = $this->sqlDataStack;
                            
                            return mysql_fetch_assoc($this->sqlResult);
                        } else {
                            $this->sqlResult = mysql_query($this->sqlQuery);
                            
                            $this->sqlArrayStack[] = $this->sqlDataStack;
                            
                            return mysql_fetch_assoc($this->sqlResult);
                        }
                    } elseif(empty($data)) {
                        if(empty($fetch)) die("invalid sql statement");
                        
                        unset($this->sqlDataStack);
                        unset($this->sqlQuery);
                        
                        $this->sqlFetchStack[] = $fetch;
                        
                        $this->sqlQuery = $fetch;
                        if($safe === true) {
                            $this->sqlResult = self::safeQuery($this->sqlQuery);
                            
                            $this->sqlArrayStack[] = $this->sqlDataStack;
                            
                            return mysql_fetch_assoc($this->sqlResult);
                        } else {
                            $this->sqlResult = mysql_query($this->sqlQuery);
                            
                            $this->sqlArrayStack[] = $this->sqlDataStack;
                            
                            return mysql_fetch_assoc($this->sqlResult);
                        }
                    }
                }
		
                /**
                 * function safeQuery
                 * 
                 * @param type $sql
                 * @return type
                 * 
                 * @final
                 */
		private function safeQuery($sql) {
			$sql  = $sql;
			$sql_ = $sql;
			$sql  = str_replace(self::$sec, "**", $sql);
                        
			if($sql != $sql_) {
				die("SQLi detected");
			} else {
				$return = mysql_query($sql_) or die(mysql_error()." - SAFE QUERY - $sql");
				return $return;
			}
		}
	}
