<?php
/**
 * FUNQuotes Security
 * 
 * @category  FUNQuotes/Security
 * @package   FUNQuotes/Engine/Security
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Engine_Security extends FUNQuotes {
        
        /**
         * @var type events
         */
        var $events;
        
        public function __construct(FUNQuotes_Engine_Events $events) {
            if(is_null($events)) {
                throw new Exception("event handler not found");
            }
            $this->events = $events;
            $this->events->fireEvent("FUN::SECURITY_CONSTRUCT");
            
            return;
        }
        
	/**
         * @var type 
         */
	public static $sec = array("1=1", "root", "mysql", "truncate", "drop database", "select", 
                                   "from", "delete", "update", "shell_exec", "kill", "~root"
                            );
            
       /**
         * @var type hash 
         */
       var $hash;
                
       /**
         * function generateHash
         * 
         * @param type $hash
         * @return type $hash
         */
	public function generateHash($hash="") {
		$this->hash = md5(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'));
                        
                return $this->hash;
	}
        
        /**
         * function getGlobal
         * 
         * @param type $var
         * @param type $mod
         * @return type
         */
	public static function getGlobal($var, $mod) {
		switch($mod) {
			case "GET":
				$global  = $_GET[$var];
				$global_ = str_replace(self::$sec, "**", $global);
				if($global != $global_) {
					die("hack detected");
				} else {
					return $global;
				}
			break;
			case "REQUEST":
				$global  = $_REQUEST[$var];
				$global_ = str_replace(self::$sec, "**", $global);
				if($global != $global_) {
					die("hack detected");
				} else {
					return $global;
				}
			break;
			case "POST":
				$global  = $_POST[$var];
				$global_ = str_replace(self::$sec, "**", $global);
				if($global != $global_) {
					die("hack detected");
				} else {
					return $global;
				}
			break;
			case "SESSION":
				$global  = $_SESSION[$var];
				$global_ = str_replace(self::$sec, "**", $global);
				if($global != $global_) {
					die("hack detected");
				} else {
					return $global;
				}
			break;
			default:
				die('Invalid argument');
			break;
		}
	}
    }