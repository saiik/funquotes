<?php
/**
 * FUNQuotes Events
 *
 * @category  FUNQuotes/Logger
 * @package   FUNQuotes/Engine/Logger
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Engine_Logger extends FUNQuotes {
        
        /**
         * @var type events
         */
        var $events;
        
        /**
         * @var type db 
         */
        var $db;
        
        /**
         * @public __construct
         * 
         * @param FUNQUotes_Engine_Database_MySQL $db
         * @param FUNQuotes_Engine_Events $events
         * @return type
         * @throws Exception
         */
        public function __construct(FUNQuotes_Engine_Events $events, FUNQuotes_Engine_Database_MySQL $db) {
            if(is_null($events)) {
                throw new Exception("event handler not found");
            }
            $this->events = $events;
            $this->events->fireEvent("FUN::LOGGER_CONSTRUCT");
            
            if(is_null($db)) {
                throw new Exception("datbase engine not found");
            }
            
            return;
        }
        
        public function addLog($m) {
            
        }
        
    }