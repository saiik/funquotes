<?php
/**
 * FUNQuotes Events
 *
 * @category  FUNQuotes/Events
 * @package   FUNQuotes/Engine/Events
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

           class FUNQuotes_Engine_Events {
	
		/**
		 * @protected eventList
		 *
		 * the list of supportet events -- do not change them !IMPORTANT!
		 **/
		var $eventList = array(
                            "FUN::CONSTRUCTOR", "FUN::DESTRUCTOR", 
                            "FUN::TEMPLATE_CONSTRUCT", "FUN::TEMPLATE_BUILD", "FUN::TEMPLATE_MODUL",
                            "FUN::SESSION_CONSTRUCT",
                            "FUN::SECURITY_CONSTRUCT",
                            "FUN::LOGGER_CONSTRUCT",
                            "FUN::START", "FUN::HEAD", "FUN::BODY", 
                            "FUN::TOP", "FUN::CONTENT", "FUN::LEFT", "FUN::RIGHT", "FUN::BOTTOM", 
                            "FUN::FOOTER", "FUN::END"
                    );
		
                /**
                 * @private sttic type 
                 */
                private $hook;
                
		/**
		 * @protected array _eventCallback 
		 * 
		 * All events will be saved here
		 **/
		public $_callbacks = array();
		
                /**
                 * @public function registerHook
                 * 
                 * @param type $hook (string)
                 * @return boolean
                 */
                public function registerHook($hook) {
                    if(empty($hook)) return false;
                    
                    $this->eventList[] = $hook;
                    
                    if(in_array($hook, $this->eventList)) {
                        return true;
                    } else {
                        return false;
                    }
                }
                
		/**
		 * @public function registerEvent 
		 * @param $event (string) - $hook (string)
		 * 
		 * registers a event with the hook (the position where the event should be fired)
		 **/
		public function registerEvent($event, $hook) {
			$rand = rand(1,1000000);
			$this->_callbacks[$rand]["event"] = $event;
			$this->_callbacks[$rand]["hook"] = $hook;
		}
		
		/**
		 * @public function fireEvent
		 * @param $hookInput (string)
		 *
		 * fires all events who has been registered at the right position
		 *
		 * @return die (if invalid hook)
		 **/
		public function fireEvent($hookInput) {
			foreach($this->_callbacks as $events) {
				$hook = $events["hook"];
				if(in_array($hook, $this->eventList)) {
				  if($hook == $hookInput):
				    call_user_func($events["event"]);
				  endif;
				} else {
                                    return die("Invalid hook");
				}
			}
		}
	}