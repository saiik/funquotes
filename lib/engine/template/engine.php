<?php
/**
 * FUNQuotes Template
 *
 * @category  FUNQuotes/Template/Engine
 * @package   FUNQuotes/Engine/Template/Engine
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    require_once 'lib/smarty/Smarty.php';

    class FUNQuotes_Engine_Template_Engine extends FUNQuotes {

        /**
         * @var type template
         */
        var $template;

        /**
         * @var type db
         */
        var $_db;

        /**
         * @var type events
         */
        var $_events;

        /**
         * @var type session
         */
        var $_session;

        /**
         * @var type security
         */
        var $_security;

        /**
         * @var type helper
         */
        var $helper;

        /**
         * @var type mobile
         */
        var $_mobile;

        /**
         * @var type moduleInformation
         */
        var $moduleInformation;

        /**
         * @public __construct
         *
         * @param FUNQuotes_Engine_Database_MySQL $db
         * @param FUNQuotes_Engine_Events $events
         * @param FUNQuotes_Engine_Session $session
         * @param FUNQuotes_Engine_Security $security
         * @param FUNQuotes_Engine_Mobile $mobile
         * @throws Exception
         *
         * @final
         */
        public function __construct(FUNQuotes_Engine_Database_MySQL $db, FUNQuotes_Engine_Events $events, FUNQuotes_Engine_Session $session, FUNQuotes_Engine_Security $security, FUNQuotes_Engine_Mobile $mobile) {
            if(is_null($db)) {
                throw new Exception("database engine not found");
            }
            if(is_null($events)) {
                throw new Exception("event handler not found");
            }
            if(is_null($session)) {
                throw new Exception("session handler not found");
            }
            if(is_null($security)) {
                throw new Exception("security class not found");
            }
            if(is_null($mobile)) {
                throw new Exception("mobile handler not found");
            }

            $this->_db          = $db;
            $this->_events      = $events;
            $this->_session     = $session;
            $this->_security    = $security;
            $this->_mobile      = $mobile;

            $this->helper       = parent::Helper("Page");

            /**
             * SMARTY CONFIG
             * !IMPORTANT!
             */
            $this->template     = new Smarty();

            $this->template->debugging      = false;
            $this->template->caching        = false;
            $this->template->cache_lifetime = 0;

            $this->template->setTemplateDir('template/default/frontend/');
            $this->template->setCompileDir('cache/template/compile/');
            $this->template->setCacheDir('cache/template/frontend/');

            $this->_session->add(
                    "TPL:SESSION:HASH",
                    $this->_security->generateHash()
            );

            $this->_events->fireEvent(
                    "FUN::TEMPLATE_CONSTRUCT"
            );
            /**
             * generates the page
             * display the page
             * !IMPORTANT!
             */
            $this->Build();
        }

        /**
         * @priate Build
         *
         * @checkLogin
         * @checkLogout
         * @getModulData
         *
         * @session get
         * @template assign
         *
         * @display
         */
        private function Build() {
            $this->checkLogin();
            $this->checkLogout();

            if($this->_session->get("USER:LOGGED") == '0') {
                $this->template->assign("isLogged", "nonLogged");
            } else {
                $this->template->assign("isLogged", "logged");

                $this->template->assign("version", parent::$version);
                $this->template->assign("build", parent::$build);

                $this->template->assign("suser", $this->_session->get("USER:USERNAME"));
                $this->template->assign("date", date("d.m.Y", time()));

                $this->template->assign("pageTitle", $this->helper->getPageTitle());

                $home_active       = $this->helper->getActive("");
                $quotes_active     = $this->helper->getActive("quotes");
                $pictures_active   = $this->helper->getActive("pictures");
                $forum_active     = $this->helper->getActive("forum");
                $chat_active       = $this->helper->getActive("chat");
                $user_active       = $this->helper->getActive("user");

                $this->template->assign("home_active", $home_active);
                $this->template->assign("quotes_active", $quotes_active);
                $this->template->assign("pictures_active", $pictures_active);
                $this->template->assign("forum_active", $forum_active);
                $this->template->assign("chat_active", $chat_active);
                $this->template->assign("user_active", $user_active);

                $this->getModulData();

                $this->_events->fireEvent("FUN::TEMPLATE_BUILD");
            }

            if($this->_mobile->isMobile()) {
                $this->displayMobile();
            } else {
                if(parent::$debug === true) $this->debug();
                else $this->display();
            }
        }

        /**
         * @private display
         *
         * @template display
         */
        private function display() {
            $this->template->display("index/index.tpl");
        }

        private function displayMobile() {
            $this->template->setTemplateDir('template/default/mobile/');
            $this->template->display("index/index.tpl");
        }

        /**
         * @private debug
         *
         * starts debug
         */
        private function debug() {
            if($this->_session->check("USER:LOGGED")) new FUNQuotes_Engine_Debug($this, $this->_events, $this->_db, $this->_session);
            else $this->display();
        }

        /**
         * @private getModulData
         *
         * @security getGlobal
         * @events fireEvent
         * @throws Exception
         */
        private function getModulData() {
            $this->_events->fireEvent("FUN::TEMPLATE_MODUL");

            $get = $this->_security->getGlobal("site", "GET");
            if(empty($get)) {
                if(class_exists("FUNQuotes_Module_Index_Index", true)) {
                    $class = new FUNQuotes_Module_Index_Index($this, $this->_db, $this->_events, $this->_security, $this->_session);
                    if(method_exists("FUNQuotes_Module_Index_Index", "init")) {
                        $class->init();
                    } else new FUNQuotes_Engine_Error("Method init not found", array(__DIR__, __FILE__, '213'), $this, $this->_events);
                } else new FUNQuotes_Engine_Error("Class FUNQuotes_Module_Index_Index not found", array(__DIR__, __FILE__, '214'), $this, $this->_events);
            } else {
                $class = "FUNQuotes_Module_".ucfirst($get)."_Index";
                if(class_exists($class, true)) {
                    $class = new $class($this, $this->_db, $this->_events, $this->_security, $this->_session);
                    if(method_exists($class, "init")) {
                        $class->init();
                        if(method_exists($class, "getInformation")) $this->moduleInformation = $class->getInformation();
                        else new FUNQuotes_Engine_Error("Method getInformation not found", array(__DIR__, __FILE__, '222'), $this, $this->_events);
                    } else new FUNQuotes_Engine_Error("Method $get not found", array(__DIR__, __FILE__, '223'), $this, $this->_events);
                } else new FUNQuotes_Engine_Error("Class $class not found", array(__DIR__, __FILE__, '224'), $this, $this->_events);
            }
        }

        /**
         * @public extendsTemplate
         *
         * @template assign
         * @param type $temp
         */
        public function extendsTemplate($temp) {
            $this->template->assign("content", $temp);
        }

        /**
         * @private checkLogin
         *
         * @session get
         * @session add
         * @securiy getGlobal
         * @db fetchRow
         * @return boolean
         */
        private function checkLogin() {
            if($this->_session->get("USER:LOGGED") === false) {
                $form = $this->_security->getGlobal("submit", "POST");
                if(!empty($form)) {
                    $user = $this->_security->getGlobal("user", "POST");
                    $pass = $this->_security->getGlobal("passwort", "POST");

                    if(empty($user) || empty($pass)) {
                        $failCounter = $this->_session->get("LOGIN:FAIL") + 1;
                        $this->_session->update("LOGIN:FAIL", $failCounter);
                        return die;
                    }

                    $passwort = $pass;
                    $hash = HASH;

                    $new_password = "";

                    for($i=0; $i<count($passwort); $i++) {
                        $new_password .= md5($hash.$passwort);
                    }

                    $new_password = md5($new_password);

                    $ds = $this->_db->fetchRow("SELECT id FROM fun_users WHERE username = '?' AND password = '?'", array($user, $new_password));
                    if(count($ds) === 1) {
                        $time = date("d.m.Y H:i", time());
                        $this->_session->add("USER:LOGGED", 1);
                        $this->_session->add("USER:USERNAME", $user);
                        $this->_session->add("USER:USERID", $ds["id"]);
                        $this->_session->add("LOGOUT:DATE", strtotime($time. '+2 minutes'));

                        header("Location: index.php");
                    } else {
                        $failCounter = $this->_session->get("LOGIN:FAIL") + 1;
                        $this->_session->update("LOGIN:FAIL", $failCounter);
                        return die('wrong user data');
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        /**
         * @private checkLogoout
         *
         * @session get
         * @session delete
         * @security getGlobal
         * @return boolean
         */
        private function checkLogout() {
            if($this->_session->get("USER:LOGGED")) {
                $action = $this->_security->getGlobal("action", "GET");
                if(empty($action)) {
                    return false;
                } else {
                    if($action == "logout") {
                        session_destroy();
                        $this->_session->delete("USER:LOGGED");
                        $this->_session->delete("USER:USERNAME");
                        $this->_session->delete("USER:USERID");
                        $this->_session->delete("LOGOUT:DATE");
                        header("Location: index.php");
                     }
                }
             }
        }
    }
