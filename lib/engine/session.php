<?php
/**
 * FUNQuotes Session
 *
 * @category  FUNQuotes/Session
 * @package   FUNQuotes/Engine/Session
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Engine_Session extends FUNQuotes {

        /**
         * @var type events
         */
        var $events;

        /**
         *
         * @return type
         */
        public function __construct(FUNQuotes_Engine_Events $events) {
            if(is_null($events)) {
                throw new Exception("event handler not found");
            }
            $this->events = $events;
            $this->events->fireEvent("FUN::SESSION_CONSTRUCT");

            return;
        }

        /**
         *
         * @param type $s
         * @param type $v
         * @return type
         */
        public function add($s, $v) {
            return self::registerSession($s, $v);
        }

        /**
         *
         * @param type $s
         * @return type
         */
        public function get($s) {
            return self::getSession($s);
        }

        /**
         *
         * @param type $s
         * @return type
         */
        public function delete($s) {
            return self::deleteSession($s);
        }

        /**
         *
         * @param type $s
         * @return type
         */
        public function check($s) {
            return self::checkSession($s);
        }

        /**
         *
         * @param type $s
         * @return type
         */
        public function update($s, $v) {
            return self::updateSession($s, $v);
        }

        /**
         *
         * @param type $s
         * @param type $v
         * @return boolean
         */
        private function registerSession($s, $v) {
            if(empty($s)) return false;
            if(empty($v)) return false;

            if(!empty($_SESSION["FUN:".$s])) return false;

            $_SESSION["FUN:".$s] = $v;

            if(empty($_SESSION["FUN:".$s])) return false;
            else return true;
        }

        /**
         *
         * @param type $s
         * @return boolean
         */
        private function getSession($s) {
            if(empty($s)) return false;

            if(empty($_SESSION["FUN:".$s])) return false;
            else return $_SESSION["FUN:".$s];
        }

        /**
         *
         * @param type $s
         * @return boolean
         */
        private function deleteSession($s) {
            if(empty($s)) return false;

            if(empty($_SESSION["FUN:".$s])) return false;
            else unset($_SESSION["FUN:".$s]);
        }

        /**
         *
         * @param type $s
         * @return boolean
         */
        private function checkSession($s) {
            if(empty($s)) return false;

            if(empty($_SESSION["FUN:".$s])) return false;
            elseif(!empty($_SESSION["FUN:".$s])) return true;
        }

        /**
         *
         * @param type $s
         * @param type $v
         * @return boolean
         */
        private function updateSession($s, $v) {
            if(empty($s)) return false;
            if(empty($v)) return false;

            if(empty($_SESSION["FUN:".$s])) return false;

            $_SESSION["FUN:".$s] = $v;

            if(empty($_SESSION["FUN:".$s])) return false;
            else return true;
        }
    }
