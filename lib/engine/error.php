<?php
/**
 * FUNQuotes Events
 *
 * @category  FUNQuotes/Error
 * @package   FUNQuotes/Engine/Error
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Engine_Error extends Exception {
        
        /**
         * @var type tpl
         */
        var $tpl;
        
        /**
         * @var type session
         */
        var $session;
        
        /**
         * @var type security
         */
        var $security;
        
        /**
         * @var type events
         */
        var $events;
        
        /**
         * @var type db
         */
        var $db;
        
        /**
         * @var type message
         */
        var $message;
        
        /**
         * @var type content
         */
        var $content;
        
        /**
         * @public __construct
         * 
         * @param type $message
         * @param type $content
         * @param FUNQuotes_Engine_Template_Engine $tpl
         * @param FUNQuotes_Engine_Events $events
         * @return type
         * @throws Exception
         */
        public function __construct($message="", $content=array(), FUNQuotes_Engine_Template_Engine $tpl, FUNQuotes_Engine_Events $events) {
            if(is_null($tpl)) {
                throw new Exception("template engine not found");
            }
            $this->tpl = $tpl;

            if(is_null($events)) {
                throw new Exception("event handler not found");
            } else {
                $this->events = $events;
            }
            
            $this->registerHooks();
            
            $this->errorMessage($message, $content);
            
            return;
        }
        
        /**
         * @public registerHooks
         * 
         * @events registerEvent
         */
        public function registerHooks() {
            $this->events->registerEvent(array("FUNQuotes_Engine_Error", "stopDisplay"), "FUN::TEMPLATE_BUILD");
        }
        
        /**
         * @public stopDisplay
         * 
         * @event trigger
         * 
         * @return type
         */
        public function stopDisplay() {
            return die;
        }

        /**
         * @public errorMessage
         * 
         * @display message, content
         * 
         * @param type $message
         * @param type $content
         */
        private function errorMessage($message, $content=array()) {
            $this->message = $message;
            $this->content = $content;
            
            $this->tpl->template->setTemplateDir('template/default/');
            
            $this->tpl->template->assign("error", $this->message);
            $this->tpl->template->assign("content", $this->content);
            
            
            $this->tpl->template->display('error.tpl');
        }
        
    }