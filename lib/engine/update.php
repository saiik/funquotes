<?php
/**
 * FUNQuotes Template
 * 
 * @category  FUNQuotes/Update
 * @package   FUNQuotes/Engine/Update
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Engine_Update extends FUNQuotes {

        var $module;
        var $updateServer;
        var $currentVersion;
        var $newVersion;
        var $downloadFile;
        var $modDir;
        var $downloadDir;
        
        public function __construct($m, $c, $u) {
            if(is_null($m)) {
                throw new Exception("no module found");
            }
            $this->module = $m;
            
            if(is_null($u)) {
                throw new Exception("no update server found");
            }
            $this->updateServer = $u;
            
            if(is_null($c)) {
                throw new Exception("no current version found");
            }
            $this->currentVersion = $c;
            
            return;
        }
        
        public function checkVersion() {
            $buildUrl = "http://".$this->updateServer."?mod=".$this->module;
            
            $this->newVersion = file_get_contents($buildUrl);
            $this->newVersion = explode(";", $this->newVersion);
            if($this->currentVersion != $this->newVersion[0]) {
                return true;
            } else {
                return false;
            }
        }
        
        public function downloadZip() {
            $this->downloadFile = "";
        }
    }