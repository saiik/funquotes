<?php
/**
 * FUNQuotes FUNQuotes Framework
 *
 * @category  FUNQuotes
 * @package   FUNQuotes/FUNQuotes
 * @version   2.0
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    function print_e($m) {
        echo "<pre>";
        print_r($m);
        echo "</pre>";
    }

    spl_autoload_register('loadFun');

    function loadFun($class) {
        $class      = strtolower($class);
        $class      = str_replace("_", "\\", $class);
        $file       = str_replace("funquotes", "", $class);
        $file_neu   = "lib".$file.".php";
        $file_mod   = $file.".php";
        $file_mod   = substr($file_mod, 1);
        
        if(file_exists($file_neu)) {
            require $file_neu;
        } else {
            if(file_exists($file_mod)) {
                require $file_mod;
            } else {
                return false;
            }
        }
    }
    
    class FUNQuotes {
	
        /**
         * @var type debug (false == off | true == on)
         */
        static $debug = false;
        
        /**
         * @static type build 
         */
        static $build = 2002;
        
        /**
         * @static type version
         */
        static $version = "2.0";
        
        /**
         * @var type events 
         */
        var $events;
        
        /**
         * @var type db 
         */
        var $db;
        
        /**
         * @var type tpl
         */
        var $tpl;
        
        /**
         * @var type session 
         */
        var $session;
        
        /**
         * @var type security 
         */
        var $security;
        
        /**
         * @var type logger
         */
        var $logger;
        
        /**
         * @var type mobile
         */
        var $mobile;
        
        public function __construct($backend=false) {
            /**
             * init database mysql
             * includes magical functions
             */
            $this->db       = new FUNQuotes_Engine_Database_MySQL(HOST, USER, PASS, BASE);
            
            /**
             * init event handler
             * register - fire events & register hook points
             */
            $this->events   = new FUNQuotes_Engine_Events();

            /**
             * fire event constructor -- including functions for funquotes construct
             */
            $this->events->fireEvent("FUN::CONSTRUCTOR");
            
            /**
             * logger - log erros - log everything
             */
            $this->logger = new FUNQuotes_Engine_Logger($this->events, $this->db);
            
            /**
             * init sessions
             * register - edit - delete sessions
             */
            $this->session  = new FUNQuotes_Engine_Session($this->events);
            
            /**
             * init security
             * check globals - generate hash 
             */
            $this->security = new FUNQuotes_Engine_Security($this->events);
            
            /**
             * init mobile
             */
            $this->mobile   = new FUNQuotes_Engine_Mobile();
            
            /**
             * initi template
             * includes smarty template engine - building the page - rendering the page
             */
            if($backend === false) $this->tpl      = new FUNQuotes_Engine_Template_Engine($this->db, $this->events, $this->session, $this->security, $this->mobile);
        }

        /**
         * @public Tpl
         * 
         * @return type
         */
        public function View() {
            return $this->tpl;
        }
        
        /**
         * @public Db
         * 
         * @return type
         */
        public function Db() {
            return $this->db;
        }
        
        /**
         * @public Events
         * 
         * @return type
         */
        public function Events() {
            return $this->events;
        }
        
        /**
         * @public Session
         * 
         * @return type
         */
        public function Session() {
            return $this->session;
        }
        
        /**
         * @public Helper
         * 
         * @param type $help
         * 
         * @return type
         */
        public function Helper($help) {
            $class = "FUNQuotes_Class_$help";
            $class = new $class($this->db, $this->events, $this->session, $this->security);
            return $class;
        }
    }