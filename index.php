<?php
/**
 * FUNQuotes Index
 *
 * @category  FUNQuotes
 * @package   FUNQuotes
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

session_start();

if(empty($_SESSION["FUN:LOGIN:FAIL"])) {
    $_SESSION["FUN:LOGIN:FAIL"] = 0;
}

print_r($_SESSION["FUN:LOGIN:FAIL"]);

if(!empty($_SESSION["FUN:LOGOUT:DATE"])) {
    if($_SESSION["FUN:LOGOUT:DATE"] < time()) {
        //session_destroy();
	//unset($_SESSION);
	//header("Location: index.php");
    }
}

require_once("./global.php");

