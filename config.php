<?php
/**
 * FUNQuotes Config
 *
 * @category  FUNQuotes
 * @package   FUNQuotes/config
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */
	if(!defined("FUN_DIR"))     define("FUN_DIR", "");
	if(!defined("FUN_LIB"))     define("FUN_LIB", FUN_DIR."/lib");

	//////////////////////////////////////////////////////////////

	define('HOST', 'localhost');
	define('USER', 'root');
	define('PASS', 'test');
	define('BASE', 'projekt');

	define('HASH', 'KFYAZSCRDCHTCKRCASM');
