<!-- version {$version} # build {$build} -->
<!DOCTYPE html>
<html>
    {block name="index_header"}
        {include file="index/header.tpl"}
    {/block}
<body>
    {if $isLogged == "logged"}
        {block name="index_top"}
            {include file="index/top.tpl"}
        {/block}
    {/if}
    {if $isLogged == "logged"}
         {include file="$content"}
     {else}
         {include file="login/index.tpl"}
     {/if}
     {block name="index_footer"}
         
     {/block}
</body>
</html>