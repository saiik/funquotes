    <div id="topBar">
        <div id="tbContent">
            {block name="index_logo"}
                <div class="left" id="logo">
                   FUNQuotes
                </div>
            {/block}
            <div class="left" id="nav">
                {block name="index_navi"}
                    <ul>
                        <li class="{$home_active}" onclick="self.location.href='index.php'">
                            HOME
                        </li>
                        <li class="{$quotes_active}" onclick="self.location.href='index.php?site=quotes'">
                            QUOTES
                        </li>
                        <li class="{$pictures_active}" onclick="self.location.href='index.php?site=pictures'">
                            PICTURES
                        </li>
                        <li class="{$forum_active}" onclick="self.location.href='index.php?site=forum'">
                            FORUM
                        </li>
                        <li class="{$chat_active}" onclick="self.location.href='index.php?site=chat'">
                            CHAT
                        </li>
                        <li class="{$user_active}" onclick="self.location.href='index.php?site=user'">
                            USER
                        </li>
                    </ul>
                {/block}
                <br style="clear: left;" />
            </div>
            <!--<div class="left" style="margin-left: 50px;">
                    <div class="bubbleBar left r">
                        M
                    </div>
                    <br style="clear: left;" />
            </div>-->
            <br style="clear: left;" />
        </div>
    </div>
    <div id="tbBorder">
        <div id="tbBorderContent">
            <div class="left">
                Welcome back {$suser} (<a href="index.php?action=logout">Logout</a>) | <a href="#" id="notifactionOpen">Benachrichtungen (0)</a>
            </div>
            <div class="right">
                <span id="zeit"></span> {$date}
            </div>
        </div>
    </div>
    <div id="topBarNoti">
        <div id="topBarNotiCont">
            Neue Nachricht von babo
        </div>
    </div>
<div id="wrapper">