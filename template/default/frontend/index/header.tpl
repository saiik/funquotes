        <head>
		<title>{block name=index_page_titel}FUN Quotes - {$pageTitle} # v2.0{/block}</title>
                {block name="index_header_meta"}
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                {/block}
                {block name="index_header_css"}
                    <link rel="stylesheet" href="template/default/resources/css/forum.css" type="text/css" />
                    <link rel="stylesheet" href="template/default/resources/css/style.css" type="text/css" />
                    <link rel="stylesheet" href="template/default/resources/css/class.css" type="text/css" />
                    <link rel="stylesheet" href="template/default/resources/css/like.css" type="text/css" />
                    <link rel="stylesheet" href="template/default/resources/css/picture.css" type="text/css" />
                    <link rel="stylesheet" href="template/default/resources/css/chat.css" type="text/css" />
                    <link rel="stylesheet" href="template/default/resources/css/quote.css" type="text/css" />
                    <link rel="stylesheet" href="template/default/resources/css/login.css" type="text/css" />
                    <link rel="stylesheet" href="template/default/resources/css/notification.css" type="text/css" />
                    <link href='http://fonts.googleapis.com/css?family=Anton' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Just+Me+Again+Down+Here' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'>
                {/block}
                {block name="index_header_javasacript"}
                    <script type="text/javascript" src="lib/js/jquery-2.0.3.min.js">

                            /*************************************/

                            /**		    LOADING JQUERY			**/

                            /*************************************/

                    </script>
                    <script src="lib/js/tinymce/tinymce.min.js"></script>
                    <!--<script type="text/javascript" src="js/fun.js"></script>-->
                    <script type="text/javascript">
                        window.setInterval("zeitanzeige()",1000);

                        function zeitanzeige()
                        {
                             d = new Date ();

                             h = (d.getHours () < 10 ? '0' + d.getHours () : d.getHours ());
                             m = (d.getMinutes () < 10 ? '0' + d.getMinutes () : d.getMinutes ());
                             s = (d.getSeconds () < 10 ? '0' + d.getSeconds () : d.getSeconds ());

                             var wochentage = new Array ("Sonntag", "Montag", "Dienstag",
                             "Mittwoch", "Donnerstag", "Freitag", "Samstag");

                             var monate = new Array ("Januar", "Februar", "März", "April",
                             "Mai", "Juni", "Juli", "August", "September",
                             "Oktober", "November", "Dezember");

                             document.getElementById("zeit").innerHTML = ''
                             + h + ':' + m + ':' + s + ', ';
                        }
                    </script>
                    <script type="text/javascript">
                            /*var refreshId = setInterval(function() {	
                                    location.reload();
                            }, 60000);*/ 
                    </script>
                {/block}
	</head>
