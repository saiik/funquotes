        <div id="quoteBox" style="width: 100%;">
            <div id="quoteBoxHead">
                <div class="left">
                    QUOTES: 
                </div>
                <div class="left" style="padding-left: 10px; padding-top: 1px; opacity: 0.7;">
                    <a href="index.php?site=quotes&e=add"><img src="template/default/resources/images/add.png" alt="" title="Quote Hinzufügen" height="24px" width="24px" /></a>
                </div>       
                <br style="clear: left;" />
            </div>
            <div id="quoteBoxContent">
                {if $singleQuote == "0"}
                {foreach from=$quotes item=data}
                <div class="quoteBoxContainer quote_{$data.id}" onclick="self.location.href='index.php?site=quotes&e=show&id={$data.id}'">
                    <div class="qbcHead">
                        <div class="left qbcHeadLeft">
                            {$data.title} 
                        </div>
                        <div class="right qbcHeadRight" style="font-size: 0.7em; padding-top: 7px; opacity: 0.4;">
                            geschrieben von {$data.user} am {$data.datum}
                        </div>
                        <br style="clear: both;" />
                    </div>
                    <div class="qbcContent">
                        {$data.content}
                    </div>
                </div>
                {/foreach}
                {else}
                    <div class="quotecontainer">
                            <div class="qhead">
                                    {$quotes.title}
                            </div>
                            <div class="qcontent">
                                   {$quotes.content}
                            </div>
                            <div class="qfoot">
                                Geschrieben von {$quotes.user} - Am {$quotes.datum}
                            </div>
                    </div>
                    <h3>Kommentare:</h3>
                    <br /><br />
                    <h3>Kommentar schreiben:</h3>
                    <form action="index.php?action=quotes&e=show&id={$quotes.id}" method="post">
                        <textarea name="comment" rows="8" cols="120"></textarea><br /><br />
                        <input type="submit" name="submit" value="Absenden!" />
                    </form>
                {/if}