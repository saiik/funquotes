        <div id="pictureBox" class="left">
            <div id="pictureBoxHead">
                PICTURES:
            </div>
            <div id="pictureBoxContent">
                {$pictures|print_r}
                {foreach from=$pictures item=data}
                    <div class="pictureBox">
                       <div class="pbImg">
                           <img src="{$data.url}" alt="" />
                       </div>
                       <div class="pbImgHover ps_{$id}">
                           <img src="template/default/images/zoom.png" alt="" />
                       </div>
                   </div>
                {/foreach}
                <br style="clear: left;" />
            </div>
    </div>