{block name="quotes_head"}
            <div id="quoteBox" class="left">
            <div id="quoteBoxHead">
                QUOTES:
            </div>
            <div id="quoteBoxContent">
{/block}
            {if $singleQuote == 1} 
                
            {else}
                {foreach from=$quotes item=data}
                    <div class="quoteBoxContainer quote_{$id}" onclick="self.location.href='index.php?site=quotes&e=show&id={$data.id}'">
                        <div class="qbcHead">
                            <div class="left qbcHeadLeft">
                                {$data.title|truncate:30} 
                            </div>
                            <div class="right qbcHeadRight" style="font-size: 0.7em; padding-top: 7px; opacity: 0.4;">
                                geschrieben von {$data.user} am {$data.datum}
                            </div>
                            <br style="clear: both;" />
                        </div>
                        <div class="qbcContent">
                            {$data.content}
                        </div>
                    </div>
                  {/foreach}
            {/if}
            </div>
        </div>