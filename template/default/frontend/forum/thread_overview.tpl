{block name="content_forum"}
   {foreach from=$data item=post}
       <div class="forum_post">
           <div id="f_user">{$post.username}</div>
           <div id="f_avatar">{$post.avatar}</div>
           <div id="f_date">{$post.date}</div>
           <div id="f_post">{$post.post}</div>     
       </div>
       {/foreach}
       <div class="f_newpost">
       <form action="?site=forum&action=controller&method=newpost" method="post">
        <textarea name="content" id="content"></textarea>
        <input type="hidden" name="threadid" value="{$data.0.thread_id}">
        <input type="submit" value="Posten">
       </form>
       </div>
{/block} 
