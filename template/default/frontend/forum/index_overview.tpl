
{block name="content_forum"}
    <div class="forum_container">
    <div class="table">
         <div class="tableHead">
            <div class="tableHeadCol" style="width: 850px;">
                Thread
            </div>
            <div class="tableHeadCol">
                User
            </div>
            <div class="tableHeadCol">
                Posts
            </div>
            <div class="tableHeadCol" style="width: 240px;">
                Datum
            </div>
            <br style="clear: left;" />
         </div>
    {foreach from=$data item=thread}
         <div class="tableContent tableContentBg{$thread.template_bg}">
            <div class="tableContentCol" style="width: 830px; text-align: left; padding-left: 20px;">
                <a href="?site=forum&action=thread&id={$thread.idfun_forum_thread}">{$thread.betreff}</a>
            </div>
            <div class="tableContentCol">
                {$thread.userid}
            </div>
            <div class="tableContentCol">
                {$thread.count}
            </div>
            <div class="tableContentCol" style="border-right: none; width: 240px;">
                {$thread.date|date_format:"%d %b %Y %H:%M"}
            </div>
            <br style="clear: left;" />
         </div> 
        {/foreach}
    </div>
</div>
{/block} 