                    <link href='http://fonts.googleapis.com/css?family=Anton' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Just+Me+Again+Down+Here' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'>
<style type="text/css">
    html, body {
    margin: 0px;
    padding: 0px;
}
body {
    font-family: Arial;
    background-color: #fafafa;
    cursor: default;
}

.left {
    float: left;
}
#topBar {
    height: 200px;
    width: 100%;
    background-color: #c02942;
    -moz-user-select:none;
}
#topBarContent {
    font-family: 'Oswald', sans-serif;
    color: #fff;
    font-size: 2.9em;
    text-align: center;
    padding-top: 80px;
    line-height: 0.5;
    width: 700px;
    margin-right: auto;
    margin-left: auto;
}
#tbcl {
    width: 400px;
    text-align: center;
    padding-left: 160px;
}
#tbcr {
    
}
.bubble {
    width: 30px;
    font-size: 0.3em;
    font-weight: bold;
    background-color: #a7dbd8;
    margin-right: 10px;
    height: 20px;
    border-radius: 20px;
    padding-top: 10px;
    opacity: 1;
    cursor: pointer;
    -moz-user-select:none;
}
#topBarContent span {
    font-family: 'Open Sans Condensed', sans-serif;
    font-size: 0.5em;
    opacity: 0.8;
}
#content {
    text-align: center;
    margin-top: 200px;
    font-family: 'Oswald', sans-serif;
    color: #999;
    font-size: 2.9em;
    opacity: 0.8;
}
#main {
    margin-top: 200px;
}
#content hr {
    border: none;
    border-bottom: 1px #999 dashed;
    width: 800px;
    margin-top: 10px;
    
}
#rForm {
    width: 700px;
    margin-left: auto;
    margin-right: auto;
    font-size: 0.5em;
    font-weight: normal;
}
#rForm .under {
    border-bottom: 1px #aaa dashed;
    text-align: left;
    margin-bottom: 30px;
}
#rForm .input {
    margin-left: 50px;
    width: 300px;
    font-size: 14px;
    color: #000;
}
#rForm input[type="submit"] {
    border: none;
    background: none;
    font-size: 1em;
    font-family: 'Oswald', sans-serif;
    color: #999;
    opacity: 0.8;
	cursor: pointer;
}
#rForm input[type="text"] {
    border: none;
    height: 30px;
    width: 390px;
    padding-left: 10px;
    background-color: #eee;
}
#rForm input[type="password"] {
    border: none;
    height: 30px;
    width: 390px;
    padding-left: 10px;
    background-color: #eee;
}
#iForm {
    width: 700px;
    margin-left: auto;
    margin-right: auto;
    font-size: 0.5em;
    font-weight: normal;
}
#iForm .iData {
    border-bottom: 1px #aaa dashed;
    width: 200px;
    text-align: left;
    margin-bottom: 30px;
    font-size: 0.7em;
}
#iForm .iDatar {
    font-size: 0.7em;
    margin-left: 100px;
}
#fForm {
    width: 700px;
    margin-left: auto;
    margin-right: auto;
    font-size: 0.5em;
    font-weight: normal;
	text-align: left;
}
#fForm hr {
	width: 700px;
}
#footer {
    height: 70px;
    background-color: #fff;
    position: absolute;
    z-index: 5;
    bottom: 0;
    width: 100%;
    text-align: center;
    -moz-user-select:none;
}
#footerContent {
    font-family: 'Oswald', sans-serif;
    font-size: 1.2em;
    opacity: 0.2;
    padding-top: 20px;
}
    </style>
<div id="topBar">
            <div id="topBarContent">
                <div id="tbcl" class="left t">
                    ERROR FUNQuotes
                </div>
            </div>
            <div id="content">
                <div id="register">
                    <div id="rForm">
                        <div class="under">
                            FATAL ERROR: {$error}
                        </div>
                        <div class="input" style="font-weight: normal; font-family: Verdana; line-height: 1.8; width: 500px; text-align: left;">
                                Folder: {$content.0}<br />
                                File: {$content.1}<br />
                                On line: {$content.2}
                        </div>
                    </div>
                </div>
           </div>
    </div>