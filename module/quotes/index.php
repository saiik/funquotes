<?php
/**
 * FUNQuotes Module Quotes
 *
 * @category  FUNQuotes/Module
 * @package   FUNQuotes/Module/Quotes
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Module_Quotes_Index extends FUNQuotes {
        
        /**
         * @var type tpl
         */
        var $tpl;
        
        /**
         * @var type db
         */
        var $db;
        
        /**
         * @var type security
         */
        var $security;
        
        /**
         * @var type session
         */
        var $session;
        
        /**
         * @var type events
         */
        var $events;
        
        /**
         * @public __construct
         * 
         * @param FUNQuotes_Engine_Template_Engine $tpl
         * @param FUNQuotes_Engine_Database_MySQL $db
         * @param FUNQuotes_Engine_Events $events
         * @param FUNQuotes_Engine_Security $security
         * @param FUNQuotes_Engine_Session $session
         * @return type
         * @throws Exception
         */
        public function __construct(FUNQuotes_Engine_Template_Engine $tpl, FUNQuotes_Engine_Database_MySQL $db, FUNQuotes_Engine_Events $events, FUNQuotes_Engine_Security $security, FUNQuotes_Engine_Session $session) {
            if(is_null($tpl)) {
                throw new Exception("template not found");
            }
            $this->tpl = $tpl;
            
            if(is_null($db)) {
                throw new Exception("database not found");
            }
            $this->db  = $db;
            
            if(is_null($security)) {
                throw new Exception("security class not found");
            }
            $this->security = $security;
            
            if(is_null($events)) {
                throw new Exception("event handler not found");
            }
            $this->events = $events;
            
            $this->events->registerHook("FUN::QUOTES_CONSTRUCT");
            $this->events->registerHook("FUN::QUOTES_TEMPLATE");
            $this->events->registerHook("FUN::QUOTES_QUOTES");
            $this->events->registerHook("FUN::QUOTES_SINGLE");
            
            $this->events->fireEvent("FUN::QUOTES_CONSTRUCT");
            
            if(is_null($session)) {
                throw new Exception("session handler not found");
            }
            $this->session = $session;
            
            return;
        }
        
        /**
         * @public getInformation
         * 
         * @return type
         */
        public function getInformation() {
            return array(
                        "version" => "1.0",
                        "updateServer" => "fun.lydgaming.de/neu/update/",
                        "label" => "Quotes",
                        "author" => "Tobias Fuchs"
                    );
        }
        
        /**
         * @public init
         * 
         * @tpl exetndsTemplate
         */
        public function init() {
            $this->tpl->extendsTemplate("quotes/index.tpl");
            $this->registerTemplate();
        }
        
        /**
         * @public registerTemplate
         * 
         * @tpl->template assign
         */
        public function registerTemplate() {
            $this->events->fireEvent("FUN::QUOTES_TEMPLATE");
            
            $action = $this->security->getGlobal("e", "GET");
            if(empty($action)) {
                $this->tpl->template->assign("singleQuote", "0");
                $this->tpl->template->assign("quotes", $this->getQuotes());
            } elseif($action == "show") {
                $id = $this->security->getGlobal("id", "GET");
                if(empty($id)) die('invalid id');
                $this->tpl->template->assign("singleQuote", "1");
                $this->tpl->template->assign("quotes", $this->getSingleQuote($id));
            } elseif($action == "add")  {
                if(empty($_POST["submit"])) {
                    $this->tpl->extendsTemplate("quotes/add.tpl");
                } else {
                    $titel = $this->security->getGlobal("titel", "POST");
                    $text = $this->security->getGlobal("text", "POST");
                    if(empty($titel) || empty($text)) {
			header("Location: index.php?site=quotes&e=add");
                    }
                    $sql = "INSERT INTO fun_quotes (title, content, userID, date) VALUES ('".$titel."', '".$text."', '".$this->session->get("USER:USERID")."', '".time()."')";
                    if($this->db->query($sql)) {
			header("Location: index.php?site=quotes");
                    } else {
			new FUNQuotes_Engine_Error("MySQL Database Error", array(__DIR__, __FILE__, '119'), $this->tpl, $this->events);
                    }
                }
            }
        }
        
        /**
         * @private getQuotes
         * 
         * @db fetchAll
         * @return type
         */
        private function getQuotes() {
            $this->events->fireEvent("FUN::QUOTES_QOUTES");
            $content_sql = $this->db->fetchAll("SELECT * FROM fun_quotes ORDER BY date DESC");
            
            $user = parent::Helper("User");
            $bbcode = parent::Helper("BBCode");
            
            $data_return = array();
            foreach($content_sql as $data) {
                $data["user"]  = $user->getUserName($data["userID"]);
                $data["content"] = $bbcode->BBParser($data["content"]);
                $data["datum"] = date("d.m.Y - H:i", $data["date"]);
                $data_return[] = $data;
            }
            return $data_return;
        }
        
        private function getSingleQuote($id) {
            $this->events->fireEvent("FUN::QUOTES_SINGLE");
            $user = parent::Helper("User");
            $bbcode = parent::Helper("BBCode");
            
            $content_sql = $this->db->fetchRow("SELECT * FROM fun_quotes WHERE id = '?'", array($id));
            $content_sql["user"]  = $user->getUserName($content_sql["userID"]);
            $content_sql["content"] = $bbcode->BBParser($content_sql["content"]);
            $content_sql["datum"] = date("d.m.Y - H:i", $content_sql["date"]);
            
            return $content_sql;
        }
    }
