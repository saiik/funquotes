<?php
/**
 * FUNQuotes Module Quotes
 *
 * @category  FUNQuotes/Module
 * @package   FUNQuotes/Module/Quotes
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Module_User_Index {
        
        /**
         * @var type tpl
         */
        var $tpl;
        
        /**
         * @var type db
         */
        var $db;
        
        /**
         * @var type security
         */
        var $security;
        
        /**
         * @public __construct
         * 
         * @param FUNQuotes_Engine_Template_Engine $tpl
         * @param FUNQuotes_Engine_Database_MySQL $db
         * @return type
         * @throws Exception
         */
        public function __construct(FUNQuotes_Engine_Template_Engine $tpl, FUNQuotes_Engine_Database_MySQL $db, FUNQuotes_Engine_Events $events, FUNQuotes_Engine_Security $security) {
            if(is_null($tpl)) {
                throw new Exception("template not found");
            }
            $this->tpl = $tpl;
            
            if(is_null($db)) {
                throw new Exception("database not found");
            }
            $this->db  = $db;
            
            if(is_null($security)) {
                throw new Exception("security class not found");
            }
            $this->security = $security;
            
            return;
        }
        
        public function getInformation() {
            return array(
                
            );
        }
        
        /**
         * @public init
         * 
         * @tpl exetndsTemplate
         */
        public function init() {
            $this->tpl->extendsTemplate("user/index.tpl");
            $this->registerTemplate();
        }
        
        /**
         * @public registerTemplate
         * 
         * @tpl->template assign
         */
        public function registerTemplate() {

        }
    }
