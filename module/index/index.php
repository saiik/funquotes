<?php
/**
 * FUNQuotes Module Home
 *
 * @category  FUNQuotes/Module
 * @package   FUNQuotes/Module/Index
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */

    class FUNQuotes_Module_Index_Index extends FUNQuotes {
        
        /**
         * @var type tpl
         */        
        var $tpl;
        
        /**
         * @var type db
         */
        var $db;
        
        /**
         * @public __construct
         * 
         * @param FUNQuotes_Engine_Template_Engine $tpl
         * @param FUNQuotes_Engine_Database_MySQL $db
         * @return type
         * @throws Exception
         */
        public function __construct(FUNQuotes_Engine_Template_Engine $tpl, FUNQuotes_Engine_Database_MySQL $db) {
            if(is_null($tpl)) {
                throw new Exception("template not found");
            }
            $this->tpl = $tpl;
            
            if(is_null($db)) {
                throw new Exception("database not found");
            }
            $this->db  = $db;
            
            return;
        }
        
        /**
         * @public init
         * 
         * @tpl exetndsTemplate
         */
        public function init() {
            $this->tpl->extendsTemplate("home/index.tpl");
            $this->registerTemplate();
        }
        
        /**
         * @public registerTemplate
         * 
         * @tpl->template assign
         */
        public function registerTemplate() {
            $this->tpl->template->assign("quotes", $this->getQuotes());
            $this->tpl->template->assign("pictures", $this->getPictures());
        }
        
        /**
         * @private getQuotes
         * 
         * @db fetchAll
         * @return type
         */
        private function getQuotes() {
            $content_sql = $this->db->fetchAll("SELECT * FROM fun_quotes ORDER BY date DESC LIMIT 0,3");
            
            $user = parent::Helper("User");
            
            $data_return = array();
            foreach($content_sql as $data) {
                $data["user"]  = $user->getUserName($data["userID"]);
                $data["datum"] = date("d.m.Y - H:i", $data["date"]);
                $data_return[] = $data;
            }
            return $data_return;
        }
        
        /**
         * @private getPictures
         * 
         * @db fetchAll
         * @return type
         */
        private function getPictures() {
            $picture_sql = $this->db->fetchAll("SELECT * FROM fun_pictures ORDER BY date DESC LIMIT 0,6");
            return $picture_sql;
        }
    }
