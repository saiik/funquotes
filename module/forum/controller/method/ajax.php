<?php



class FUNQuotes_Module_forum_controller_method_ajax{
    
    var $db;
    var $tpl;
    
    public function __construct(FUNQuotes_Engine_Template_Engine $tpl, FUNQuotes_Engine_Database_MySQL $db) {
            if(is_null($tpl)) {
                throw new Exception("template not found");
            }
            $this->tpl = $tpl;
            
            if(is_null($db)) {
                throw new Exception("database not found");
            }
            $this->db  = $db;
            
            return;
        }
        
      
      private function getmethod(){
           
          if(!empty($_POST['method'])){$method = $_POST['method'];}
          if(!empty($_GET['method'])){$method = $_GET['method'];} 
           
           return $method;
       }  
       
      private function setfunc(){
          
          $method = self::getmethod();
          
          switch($method){
              
              case "newthread":
                  $data = self::newthread();
                  break;
              case "newpost":
                  $data = self::newpost();
                  break;
              default:
                    break;
              
          }
          return $data;
          
      } 
      
     private function getthreadid(){
           
          if(!empty($_POST['threadid'])){$threadid = $_POST['threadid'];}
          if(!empty($_GET['threadid'])){$threadid = $_GET['threadid'];} 
           
           return $threadid;
       }
      private function newpost(){
          $content = $_REQUEST['content'];
          $userid = $_SESSION['FUN:USER:USERID'];
          $threadid = self::getthreadid();
          self::post($threadid,$userid,$content);
      }
      
      private function post($id,$userid,$content){
          $time = time();
          if(!empty($userid) && !empty($content) && !empty($id) ){
          $sql ="INSERT INTO `projekt`.`fun_forum_thread_post` (`thread_id`, `user_id`, `post`, `date`) VALUES ('".$id."', '".$userid."', '".$content."', '".$time."')";
          $this->db->query($sql);
          }
          
          return $id;
    }   
      private function newthread(){
          
         $time = time();
         $userid = $_SESSION['FUN:USER:USERID'];
         $content = $_REQUEST['content'];
         $betreff = $_REQUEST['betreff'];
         if(!empty($userid) && !empty($content) && !empty($betreff) ){
         $sql ="INSERT INTO `projekt`.`fun_forum_thread` (`betreff`, `userid`, `date`) VALUES ('".$betreff."', '".$userid."', '".$time."')";
         $this->db->query($sql);
         $sql = "select idfun_forum_thread from fun_forum_thread order by idfun_forum_thread desc";
         $data = $this->db->fetchRow($sql);
         $id = $data['idfun_forum_thread'];
         self::post($id,$userid,$content);
         }
         $data = array();
         $data['id'] = $id;
         
         return $data;
         
      } 
        
      
      public function getdata(){
          $data = self::setfunc();
          return $data;
      }
        
        
        
        
        
}
?>
