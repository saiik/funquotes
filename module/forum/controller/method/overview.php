<?php



class FUNQuotes_Module_forum_controller_method_overview{
    
    var $db;
    var $tpl;
    
    public function __construct(FUNQuotes_Engine_Template_Engine $tpl, FUNQuotes_Engine_Database_MySQL $db) {
            if(is_null($tpl)) {
                throw new Exception("template not found");
            }
            $this->tpl = $tpl;
            
            if(is_null($db)) {
                throw new Exception("database not found");
            }
            $this->db  = $db;
            
            return;
        }
        
        
        
      public function getdata(){
          
          $sql ="select *,(select count(*) from fun_forum_thread_post where fun_forum_thread.idfun_forum_thread = fun_forum_thread_post.thread_id) as count from fun_forum_thread";
          $data = $this->db->fetchAll($sql);
          return $data;
          
      }  
        
        
        
        
}
?>
