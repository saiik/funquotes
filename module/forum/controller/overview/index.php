<?php



class FUNQuotes_Module_forum_controller_overview_index{
    
    var $db;
    var $tpl;
    
    public function __construct(FUNQuotes_Engine_Template_Engine $tpl, FUNQuotes_Engine_Database_MySQL $db) {
            if(is_null($tpl)) {
                throw new Exception("template not found");
            }
            $this->tpl = $tpl;
            
            if(is_null($db)) {
                throw new Exception("database not found");
            }
            $this->db  = $db;
            
            return;
        }
        

      public function getdata(){
          
          $sql ="select * from fun_forum_thread";
          $data = $this->db->fetchAll($sql);
          $i = 0;
          foreach($data as $item) {
              if($i%2) {
                  $item["template_bg"] = "1";
              } else {
                  $item["template_bg"] = "2";
              }
              $data_full[] = $item;
          
              $i++;
          }
          return $data_full;
          
      }  
        
        
        
        
}
?>
