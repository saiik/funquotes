<?php
/**
 * FUNQuotes Module Home
 *
 * @category  FUNQuotes/Module
 * @package   FUNQuotes/Module/Index
 * @author    Marvin Janknecht <marvinjan@gmx.de>
 * @copyright Copyright (c) 2013, Marvin Janknecht
 */

    class FUNQuotes_Module_forum_controller_controller{
        
        /**
         * @var type tpl
         */        
        var $tpl;
        
        /**
         * @var type db
         */
        var $db;
        
        /**
         * @public __construct
         * 
         * @param FUNQuotes_Engine_Template_Engine $tpl
         * @param FUNQuotes_Engine_Database_MySQL $db
         * @return type
         * @throws Exception
         */
        public function __construct(FUNQuotes_Engine_Template_Engine $tpl, FUNQuotes_Engine_Database_MySQL $db) {
            if(is_null($tpl)) {
                throw new Exception("template not found");
            }
            $this->tpl = $tpl;
            
            if(is_null($db)) {
                throw new Exception("database not found");
            }
            $this->db  = $db;
            
            return;
        }
        
       
       
       private function getaction(){
           
          if(!empty($_POST['action'])){$action = $_POST['action'];}
          if(!empty($_GET['action'])){$action = $_GET['action'];} 
           
           return $action;
       }
       
       private function builddata($data = NULL){
           
           $action = self::getaction();
           
           switch($action){
               
               case 'thread':
                   $con = new FUNQuotes_Module_forum_controller_thread_index($this->tpl,$this->db);
                   $data = $con->getdata();
                    break;
               default:
                  $con = new FUNQuotes_Module_forum_controller_overview_index($this->tpl,$this->db);
                  $data = $con->getdata();
                    break;
           }
 
           return $data;
       }
       
       private function template($action,$array){
           $page = array();
           $page['action'] = $action;
           $page['data'] = $array;
           
           return $page;
       }
       
       public function gettemplate(){
           return self::template(self::getaction(),self::builddata());
       }
       
    }
