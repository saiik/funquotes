<?php
/**
 * FUNQuotes Module Home
 *
 * @category  FUNQuotes/Module
 * @package   FUNQuotes/Module/Index
 * @author    Marvin Janknecht <marvinjan@gmx.de>
 * @copyright Copyright (c) 2013, Marvin Janknecht
 */

    class FUNQuotes_Module_forum_Index {
        
        
        var $data;
        /**
         * @var type tpl
         */        
        var $tpl;
        
        /**
         * @var type db
         */
        var $db;
        
        var $action;
        
        /**
         * @public __construct
         * 
         * @param FUNQuotes_Engine_Template_Engine $tpl
         * @param FUNQuotes_Engine_Database_MySQL $db
         * @return type
         * @throws Exception
         */
        public function __construct($tpl, $db) {
            if(is_null($tpl)) {
                throw new Exception("template not found");
            }
            $this->tpl = $tpl;
            
            if(is_null($db)) {
                throw new Exception("database not found");
            }
            $this->db  = $db;
            
            return;
        }
        
        /**
         * @public getInformation
         * 
         * @return type
         */
        public function getInformation() {
            return array(
                        "version" => "1.0",
                        "label" => "Forum",
                        "updateServer" => "fun.lydgaming.de/neu/update/",
                        "author" => "Marvin Janknecht"
                    );
        }
        
        /**
         * @public init
         * 
         * @tpl exetndsTemplate
         */
        public function init() {
            
            $controller = new FUNQuotes_Module_forum_controller_controller($this->tpl,$this->db);
            $template = $controller->gettemplate();
            $this->data = $template['data'];
            $this->action = $template['action'];

            $this->tpl->extendsTemplate("forum/index.tpl");
            if(!empty($this->action)){$this->tpl->extendsTemplate("forum/".$this->action.".tpl");}
            $this->registerTemplate();
        }
        
        /**
         * @public registerTemplate
         * 
         * @tpl->template assign
         */
        public function registerTemplate() {
            $this->tpl->template->assign("data", $this->data);
            $this->tpl->template->assign("action",$this->action);
        }
       
    }
