<?php
/**
 * FUNQuotes Module Home
 *
 * @category  FUNQuotes/Module
 * @package   FUNQuotes/Module/Index
 * @author    Marvin Janknecht <marvinjan@gmx.de>
 * @copyright Copyright (c) 2013, Marvin Janknecht
 */

    class FUNQuotes_Module_chat_Index {
        
        
        var $data;
        /**
         * @var type tpl
         */        
        var $tpl;
        
        /**
         * @var type db
         */
        var $db;
        
        var $action;
        
        /**
         * @public __construct
         * 
         * @param FUNQuotes_Engine_Template_Engine $tpl
         * @param FUNQuotes_Engine_Database_MySQL $db
         * @return type
         * @throws Exception
         */
        public function __construct($tpl, $db) {
            if(is_null($tpl)) {
                throw new Exception("template not found");
            }
            $this->tpl = $tpl;
            
            if(is_null($db)) {
                throw new Exception("database not found");
            }
            $this->db  = $db;
            
            return;
        }
        
        /**
         * @public init
         * 
         * @tpl exetndsTemplate
         */
        public function init() {

            $this->tpl->extendsTemplate("chat/index.tpl");
            $this->registerTemplate();
        }
        
        /**
         * @public registerTemplate
         * 
         * @tpl->template assign
         */
        public function registerTemplate() {
            $this->tpl->template->assign("data", $this->data);
        }
       
    }
