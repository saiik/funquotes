<?php
/**
 * FUNQuotes Global
 *
 * @category  FUNQuotes
 * @package   FUNQuotes
 * @author    Tobias Fuchs <saikon@hotmail.de>
 * @copyright Copyright (c) 2013, Tobias Fuchs (http://slymedia.bplaced.net)
 */
	require_once("./config.php");
	
	require_once(FUN_LIB."/funquotes.php");
	
	new FUNQuotes();